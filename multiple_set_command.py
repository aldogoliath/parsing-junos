#!/usr/local/bin/python
# -*- coding: utf-8 -*-
#Code by @aldogoliath --- 12-Jul-2016
import string
import re
import os
import argparse

output_filename = {}
target_config = []

def get_target_path():
    parser = argparse.ArgumentParser()
    parser.add_argument('target_dir', type=str,
        help='Directory where the target config files are stored')
    parser.add_argument('-s', '--setdir',
        help='Directory where the set style config files will be stored', action='store_true')
    args = parser.parse_args()
    if args.setdir:
        set_directory = raw_input("Insert the set command files' directory:" + '\n')
    else:
        print "By default we store the output (set command files) @ %s/set_command_configs directory" % args.target_dir
        set_directory = args.target_dir + 'set_command_configs/'
    return args.target_dir, set_directory

def get_target_files(target_path_):
    return [ file for file in os.listdir(target_path_) if file.endswith('txt') ]

def get_output_files(target_config_):
    output_files = {}
    for element in target_config_:
        if element.count('_') > 3:
            output_files[element] = string.join(element.split('_')[:-2],'_') + '_set.txt'
        else:
            output_files[element] = element.split('.txt')[0] + '_set.txt'
    return output_files

def process_data(target_config,set_configs_path,output_filename,target_path):
    inactive_regex = re.compile('.*(inactive:).*')
    for target in target_config:
        buff = []
        inactive_element = ''
        if set_configs_path != '.':
            output_file_path = set_configs_path+output_filename[target]
        else:
            output_file_path = output_filename[target]

        with open(target_path+target,'r') as config_file:
            data = config_file.readlines()

        print 'Now working on:', target

        with open(output_file_path,'w+') as output_set_file:
            for line in data:
                if line[0] == '#' or 'show config' in line: continue
                if inactive_regex.search(line):
                    line = line.replace('inactive:','')
                    inactive_element = line.strip()
                if '{\n' in line:
                    buff.append(line[:-2].strip())
                else:
                    if ';' in line:
                        line = line.strip()
                        if 'SECRET-DATA' in line:
                            output_set_file.write('set '+ string.join(buff) + ' ' + line[:line.index(';')] + '\n')
                        else:
                            output_set_file.write('set '+ string.join(buff) + ' ' + line[:-1] + '\n')
                    elif '}\n' in line:
                        if len(buff) > 0:
                            if buff[-1] in inactive_element:
                                output_set_file.write('deactivate '+ string.join(buff) + '\n')
                                inactive_element = ''
                            buff = buff[:-1]

def main(set_configs_dir=''):
    target_path, set_configs_path = get_target_path()
    target_config = get_target_files(target_path)
    output_filename = get_output_files(target_config[:])
    process_data(target_config,set_configs_path,output_filename,target_path)

if __name__ == '__main__':
    main()
